from apis.base_api import BaseAPI
from constants import RequestType
import logger
import pprint

class StudentDownloadCourseAPI(BaseAPI):
    request_type = RequestType.get
    resource = '/yosemite/courseware/student_download'

    def __init__(self, proxy, username, password, args):
        super(StudentDownloadCourseAPI, self).__init__(proxy, username, password, args, None)
        self.run_request()

    def _get_courseware_download_dict_list(self):
        result = self.body['result']
        pprint.pprint(result, indent=2)
        return result['urls'] if 'urls' in result else []

    def get_courseware_download_dict_list(self):
        download_dict = [{'url': self.body['result']['download_url'], u'frequency': 0, u'speed': 0}]
        download_dict.extend(self._get_courseware_download_dict_list())
        return download_dict

    def get_md5(self):
        return self.body['result']['md5']

