import unittest
import requests
import time
import hashlib
from ddt import ddt, data
import os
import importlib.util

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
settings_module_file_path = os.path.join(BASE_DIR, 'settings.py')

settings_spec = importlib.util.spec_from_file_location('settings', settings_module_file_path)
settings_module = importlib.util.module_from_spec(settings_spec)
settings_spec.loader.exec_module(settings_module)

RESOURCE_ROOT_URL_BASE_PATH = settings_module.RESOURCE_ROOT_URL_BASE_PATH

node = settings_module.NODE

headers = settings_module.headers

courceware_list = settings_module.courseware_list

courceware_list_result = {'{0}-{1}-{2}'.format(cw['level'], cw['unit'], cw['lesson']): cw for cw in courceware_list}

fields_map = settings_module.fields_map

@ddt
class CourseResourceTest(unittest.TestCase):

    def setUp(self):
        pass

    @data(*courceware_list_result.items())
    def test_download_and_verify_md5(self, courseware):
        self.global_start_time = time.time()
        self.current_level_unit_lesson = courseware[0]
        self.courseware = courseware[1]
        level = self.courseware['level']
        unit = self.courseware['unit']
        lesson = self.courseware['lesson']
        self.url = node + RESOURCE_ROOT_URL_BASE_PATH.format(level=level, unit=unit, lesson=lesson)
        courceware_list_result[self.current_level_unit_lesson]['resource_url'] = self.url
        self.get_resource_info()

    def get_resource_info(self):
        start_time = time.time()
        r = requests.get(self.url)
        self.assertEqual(r.status_code, 200, '获取资源网络请求失败[{url}]'.format(url=self.url))
        #
        resp = r.json()
        end_time = time.time()
        courceware_list_result[self.current_level_unit_lesson]['get_resource_cost'] = end_time - start_time
        self.assertEqual(resp['code'], 0, '接口返回异常[{url}][{json}]'.format(url=self.url, json=r.text))
        #
        self.courseware = resp['result']['courseware']
        self.assertEqual(len(self.courseware['urls']), 2, '课件下载链接数量错误[{count}]'.format(count=len(self.courseware['urls'])))
        self.code = resp['result']['code']
        self.assertEqual(len(self.code['urls']), 2, '代码下载链接数量错误[{count}]'.format(count=len(self.code['urls'])))
        #
        first_cw = courceware_list[0]
        if self.current_level_unit_lesson == '{0}-{1}-{2}'.format(first_cw['level'], first_cw['unit'], first_cw['lesson']):
            self.get_resource('code')
        self.get_resource('courseware')

    def get_resource(self, resource):
        self.resource = getattr(self, resource)
        self.try_download_link(resource, 'inter')
        self.try_download_link(resource, 'next')
        self.global_end_time = time.time()

    def try_download_link(self, resource, tag):
        md5 = self.resource['md5']
        download_link = None
        if tag == 'inter':
            download_link = self.resource['urls'][0]['url']
        elif tag == 'next':
            download_link = self.resource['urls'][1]['url']
        if download_link is None:
            return

        start_time = time.time()
        with requests.get(download_link, stream=True, headers=headers) as response: # 开启一次下载
            downloaded = b""
            for chunk in response.iter_content(chunk_size=10240000):  # 设置下载分片10M
                downloaded += chunk
                # current_time = time.time()
                # speed = len(downloaded) / 1024 / (current_time - start_time)  # 每下载一个分块，都统计一下下载时间和速度
                # if not speed_line or current_time - speed_line[-1]['timestamp'] > 1:  # 如果记录速度的列表为空，或当前时间比记录列表里最后一个的时间至少多一分钟时，记录列表里加入当前时间和速度
                #     speed_line.append({'timestamp': current_time, 'speed': speed})
                #     if current_time - start_time >= 5:  # 如果下载到第5秒以后，统计记录列表里速度大于500k的次数，有一次大于500k，就标记退出尝试循环
                #         speed_greater_500_count = len([e['speed'] for e in speed_line if e['speed'] > 500])
                #         if speed_greater_500_count > 0:
                #             exit_try = True
            end_time = time.time()
            courceware_list_result[self.current_level_unit_lesson]['download_{0}_{1}_cost'.format(resource, tag)] = end_time - start_time
            hasher = hashlib.md5()
            hasher.update(downloaded)
            downloaded_md5 = hasher.hexdigest().upper()
            if downloaded_md5 == md5:
                courceware_list_result[self.current_level_unit_lesson]['download_{0}_{1}_md5_match'.format(resource, tag)] = True
            else:
                courceware_list_result[self.current_level_unit_lesson]['download_{0}_{1}_md5_match'.format(resource, tag)] = False
            self.assertGreaterEqual(downloaded_md5, md5, '下载到的资源文件MD5不匹配[{old_md5}|{geted_md5}]'.format(old_md5=md5, geted_md5=downloaded_md5))

    @classmethod
    def tearDownClass(cls):
        report = []
        clr = courceware_list_result
        for k, v in clr.items():
            report.append(k)
            for attr, value in v.items():
                report.append('\t'+fields_map.get(attr, attr)+':'+str(value))
        #输出
        report_file_path = os.path.join(BASE_DIR, 'report_dir', time.strftime('%Y-%m-%d_%H:%M:%S')+'.txt')
        with open(report_file_path, 'w') as f:
            f.write('\n'.join(report))


if __name__ == '__main___':
    if not os.path.exists(os.path.join(BASE_DIR, 'report_dir')):
        os.mkdir(os.path.join(BASE_DIR, 'report_dir'))
    unittest.main()
