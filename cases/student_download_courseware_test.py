import os
import sys
import pytest


import logger
from apis.student_download_courseware import StudentDownloadCourseAPI
from utils import get_remote_md5
from cases.courseware_config import courceware_list


@pytest.mark.parametrize("courseware_arg", courceware_list)
def test_courseware_md5(courseware_arg):
    logger.info("running test case: {}".format(courseware_arg))
    username = ''
    password = ''
    courseware_list_api = StudentDownloadCourseAPI({}, username, password, args=courseware_arg)
    assert courseware_list_api.return_code == 0
    download_dict_list = courseware_list_api.get_courseware_download_dict_list()
    result = {download_dict['url']: False for download_dict in download_dict_list}
    for download_url in result:
        logger.debug('checking url: {}'.format(download_url))
        try:
            md5 = get_remote_md5(download_url)
            if md5 == courseware_list_api.get_md5():
                result[download_url] = True
                break
        except Exception:
            continue
    assert any(result.values())
