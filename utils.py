import hashlib
import urllib2

from constants import MAX_FILE_SIZE
from datetime import datetime
import logger


def get_remote_md5(url):
    remote = urllib2.urlopen(url)
    hash = hashlib.md5()

    total_read = 0
    start = datetime.now()
    while total_read < MAX_FILE_SIZE:
        data = remote.read(4096)
        total_read += 4096

        if not data:
            break

        hash.update(data)
        #logger.info('{} seconds elapsed'.format(datetime.now() - start))
    time_delta = datetime.now() - start
    logger.info("total: {}".format(time_delta))
    used_seconds = time_delta.seconds
    if used_seconds > 30:
        raise Exception("Download time exceed 10 seconds")
    return hash.hexdigest()


def check_have_value(key, value):
    if not value:
        raise RuntimeError('[{}]: should have value'.format(key))

