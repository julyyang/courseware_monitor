class RequestType(object):
    get = 'GET'
    post = 'POST'


MAX_FILE_SIZE = 100 * 1024 * 1024
