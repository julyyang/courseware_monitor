import json
import urllib
import urllib2

import logger
import settings
from constants import RequestType
from utils import check_have_value


class BaseAPI(object):
    request_type = None
    resource = ''

    def __init__(self, proxy, username, password, args, post_data):
        self.username = username
        self.password = password
        self.args = args
        self.base_url = settings.BASE_URL
        self.post_data = post_data
        self.proxy = proxy
        self.response = None
        self.body = None
        self.return_code = -1

    def _parse_response_body(self):
        return json.JSONDecoder().decode(self.response)

    def _check_request_params(self, request_type):  # TODO: uncomment
        if request_type == RequestType.get:
            check_have_value('args', self.args)
        if request_type == RequestType.post:
            check_have_value('post data', self.post_data)

    def _build_url(self):
        return '{url}{resource}?{params}'.format(
            url=self.base_url, resource=self.resource, params=urllib.urlencode(self.args)) if self.args else \
            self.base_url

    def _build_proxy(self):
        return urllib2.ProxyHandler(self.proxy)

    def run_request(self):
        self._check_request_params(self.request_type)
        headers = [
            ('Accept', 'application/json'),
        ]
        if self.proxy:
            opener = urllib2.build_opener(CustomerizedHttpErrorHandler, self._build_proxy())
        else:
            opener = urllib2.build_opener(CustomerizedHttpErrorHandler)

        opener.addheaders = headers

        urllib2.install_opener(opener)

        if self.request_type == RequestType.post:
            content = urllib2.urlopen(self._build_url(), data=urllib.urlencode(self.post_data))
        elif self.request_type == RequestType.get:
            content = urllib2.urlopen(self._build_url())
        else:
            raise RuntimeError('not supported request type: [{}]'.format(self.request_type))

        self.response = content.read()
        self.body = self._parse_response_body()
        self.return_code = self.body['code']


class CustomerizedHttpErrorHandler(urllib2.BaseHandler):

    def http_error_429(self, request, response, code, msg, hdrs):
        return response

    def http_error_default(self, request, response, code, msg, hdrs):
        logger.error('[{}] - [{}]'.format(code, msg))
        raise RuntimeError('error when request the rest api')


