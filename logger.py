import time

import settings

LOG_LEVEL_DICT = {
    'DEBUG': 0,
    'INFO': 1,
    'WARN': 2,
    'ERROR': 3,
}


def _get_log_level():
    return LOG_LEVEL_DICT[settings.LOGGING_LEVEL]


def _time():
    return time.strftime("%Y-%m-%d %H:%M:%S")


def debug(line):
    if _get_log_level() <= LOG_LEVEL_DICT['DEBUG']:
        print u'DEBUG - {} - {}'.format(_time(), line)


def info(line):
    if _get_log_level() <= LOG_LEVEL_DICT['INFO']:
        print u'INFO - {} - {}'.format(_time(), line)


def warn(line):
    if _get_log_level() <= LOG_LEVEL_DICT['WARN']:
        print u'WARN - {} - {}'.format(_time(), line)


def error(line):
    if _get_log_level() <= LOG_LEVEL_DICT['ERROR']:
        print u'ERROR - {} - {}'.format(_time(), line)
