from apis.base_api import BaseAPI
from constants import RequestType


class TestAPI(BaseAPI):
    request_type = RequestType.get
    resource = ''

    def __init__(self, proxy, username, password, args):
        super(TestAPI, self).__init__(proxy, username, password, args, None)
        self.run_request()

    def get_body(self):
        return self.body

    def get_return_code(self):
        pass
